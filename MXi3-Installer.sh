#!/bin/bash
read -p "Please enter your username: " name

# Update Repositories
sudo apt-get update -y

# Copy relevant Configuration Files
mv MX-Scripts ~/
cp ~/MX-Scripts/conky-i3bar ~/ && cp ~/MX-Scripts/.conkyrci3 ~/ && cp -Rf ~/MX-Scripts/.config ~/ && cp ~/MX-Scripts/.bashrc ~/
cp -Rf ~/MX-Scripts/.epsxe ~/ && cp -Rf ~/MX-Scripts/.fonts ~/ && cp -Rf ~/MX-Scripts/.urxvt ~/
cp -Rf ~/MX-Scripts/.vimrc ~/ && cp -Rf ~/MX-Scripts/.Xresources ~/ && cp -Rf ~/MX-Scripts/.xinitrc ~/ 

# Copy same files to etc/skel
sudo cp ~/MX-Scripts/conky-i3bar /etc/skel/ && sudo cp ~/MX-Scripts/.conkyrci3 /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.config /etc/skel/
sudo cp -Rf ~/MX-Scripts/.epsxe /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.fonts /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.urxvt /etc/skel/
sudo cp -Rf ~/MX-Scripts/.vimrc /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.Xresources /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.xinitrc /etc/skel/
sudo cp -Rf ~/MX-Scripts/.bashrc /etc/skel/ 

# Installing VirtualBox
sudo apt install virtualbox virtualbox-dkms virtualbox-ext-pack vde2 qemu qemu-kvm -y
# Installing Virtualisation
sudo apt install virtualbox-guest-utils virtualbox-guest-dkms virtualbox-guest-additions-iso open-vm-tools open-vm-tools-desktop dkms build-essential linux-headers-$(uname -r) -y

# Installing Java 8 for Minecraft
sudo apt-get install default-jdk java-package gdebi wget -y
# Install i3-System and programs
sudo apt-get install gdebi polybar i3-wm rofi nitrogen compton galternatives rxvt-unicode vim vim-syntax-gtk asunder aac-enc tmux ranger steam ncmpcpp mpd libmpdclient2 mpv lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings -y
sudo rm /etc/mpd.conf
sudo apt-get install radiotray w3m w3m-img -y

# Clone and compile Emulation Station
git clone https://www.github.com/Aloshi/EmulationStation.git
sudo apt install libsdl2-dev libboost-system-dev libboost-filesystem-dev libboost-date-time-dev libboost-locale-dev libfreeimage-dev libfreetype6-dev libeigen3-dev libcurl4-openssl-dev libasound2-dev libgl1-mesa-dev build-essential cmake fonts-droid-fallback
cd EmulationStation
cmake .
make
cd .. 
rm -r EmulationStation

# install i3-gaps dependencies 
sudo apt-get install cmake meson dh-autoreconf libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev libxcb-icccm4-dev libyajl-dev -y
sudo apt-get install libev-dev libxcb-xkb-dev libxcb-cursor-dev libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev libstartup-notification0-dev -y 
sudo apt-get install libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev libxcb-shape0 libxcb-shape0-dev -y

# clone the repository
cd ~/
git clone https://www.github.com/Airblader/i3 i3-gaps
cd i3-gaps

# compile & install
rm -rf build/
mkdir -p build && cd build/
meson --prefix /usr/local
ninja
sudo ninja install
cd ../..

# Download and Install Steam
#get https://cdn.cloudflare.steamstatic.com/client/installer/steam.deb
#sudo gdebi steam.deb
#rm steam.deb
# Remove libreoffice
sudo apt remove -y libnumbertext-1.0-0 libnumbertext-data libreoffice-avmedia-backend-gstreamer libreoffice-base libreoffice-base-core 
sudo apt remove -y libreoffice-base-drivers libreoffice-calc libreoffice-common libreoffice-core libreoffice-draw libreoffice-gnome libreoffice-gtk3 
sudo apt remove -y libreoffice-help-common libreoffice-help-en-us libreoffice-impress libreoffice-java-common libreoffice-math libreoffice-report-builder-bin 
sudo apt remove -y libreoffice-sdbc-hsqldb libreoffice-style-colibre libreoffice-style-tango libreoffice-writer lo-main-helper mugshot mythes-en-us 
sudo apt remove -y printer-driver-cups-pdf uno-libs3 ure
#sudo reboot
#sudo apt-get update && sudo apt-get upgrade

# Add Polybar Configs
sudo rm -Rf ~/i3-gaps
sudo rm -Rf ~/polybar 
chmod +x ~/.config/polybar/launch.sh
sudo apt-get update -y


# Add IceSSB
sudo apt install devscripts -y
cd ~/mx-i3
sudo ice_5.3.0_all.deb
sudo sed -i 's/#autologin-session=UNIMPLEMENTED/autologin-session=i3/g' /etc/lightdm/lightdm.conf
sudo sed -i "s/autologin-user=demo/autologin-user=$name/g" /etc/lightdm/lightdm.conf
sudo rm -Rf ~/MX-Scripts && sudo rm -Rf ~/mx-i3

# Flatpak Installs
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.mojang.Minecraft
#flatpak install flathub com.valvesoftware.Steam
sudo apt upgrade -y
sudo apt autoremove -y
minstall-pkexec &
