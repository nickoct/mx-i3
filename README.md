**MX-i3**

This is my personal script and configs to change MX Linux from Xfce4 to i3-gaps
To initiate script type sudo chmod +x MXi3-Installer.sh in the terminal.
Then type ./MXi3-Installer.sh and leave the script to do the rest.
When it first starts it will ask you to enter your username.  Please enter the
username you will use for your new MX Linux install and press enter.
Every so often you will be prompted to reply to on screen prompts.
I recently added zfs to the script if you don't want it just remove it before
running it. 
This script is to be run before installing MX-Linux as the MX installer will be
started at the end of the script.  Make sure if you wish to manually partition
your harddrive do it before.  I use Gdisk but feel free to use your partitioner
of choice.  Thanks and hope you enjoy it.
                    Mod Keys
                    $mod = Windows Key
                    F12 = Xfce Drop Down Terminal
                    $mod+m = Starts Minecraft
                    $mod+Return = Opens a URxvt Terminal
                    $mod+d = Starts Rofi Application Launcher
                    $mod+Shift+BackSpace = Reboots System
                    $mod+Shift+x = Shutdown System
                    Dropbox is Uncommented out
                    Polybar is installed
                    Radio-Tray is installed